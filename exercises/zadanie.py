import random
import datetime

female_name = ["Katarzyna","Aleksandra","Anna","Agnieszka","Żaneta"]
male_name = ["Marcin","Filip","Tomasz","Marek","Jakub"]
last_name = ["Nowak","Janda","Baranek","Janik","Urbanek"]
country = ["Poland","Denmark","Spain","Norway","Finland"]
today = datetime.date.today()
year = today.year


def get_list_of_female_and_male_objects(name_female, name_male, surname, where_from,):

    list_of_male_and_female_objects = []

    for i in range(10):
        if len(list_of_male_and_female_objects) < 5:
            firstname = random.choice(name_female)
            lastname = random.choice(surname)
            random_age = random.randint(5, 45)

            if random_age >= 18:
                adult = True
            else:
                adult = False

            birth_year = year - random_age

            female_personal_data = {
                "firstname": firstname,
                "lastname": lastname,
                "country": random.choice(where_from),
                "email": f"{firstname.lower()}.{lastname.lower()}@example.com",
                "age": random_age,
                "adult": adult,
                "birth_year": birth_year
            }
            list_of_male_and_female_objects.append(female_personal_data)

        else:

            firstname = random.choice(name_male)
            lastname = random.choice(surname)
            random_age = random.randint(5, 45)

            if random_age >= 18:
                adult = True
            else:
                adult = False

            birth_year = year - random_age

            female_personal_data = {
                "firstname": firstname,
                "lastname": lastname,
                "country": random.choice(where_from),
                "email": f"{firstname.lower()}.{lastname.lower()}@example.com",
                "age": random_age,
                "adult": adult,
                "birth_year": birth_year
                }
            list_of_male_and_female_objects.append(female_personal_data)

    return list_of_male_and_female_objects


if __name__ == '__main__':

    final_list = get_list_of_female_and_male_objects(female_name, male_name ,last_name, country)

    for n in final_list:
        print(f"HI ! I'm {n['firstname']} {n['lastname']}. I come from {n['country']} and I was born in {n['birth_year']}")